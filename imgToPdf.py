import fpdf
from PIL import Image
import sys
from os import listdir
from os.path import isfile, join

print(sys.argv)

directory=sys.argv[1]
output_name=sys.argv[2]

pdf=fpdf.FPDF()

files=[f for f in listdir(directory) if isfile(join(directory, f))]
for file in files:
    file=join(directory, file)
    print(file)
    cover=Image.open(file)
    width,height=cover.size
    width, height = float(width * 0.264583), float(height * 0.264583)

    # given we are working with A4 format size 
    pdf_size = {'P': {'w': 210, 'h': 297}, 'L': {'w': 297, 'h': 210}}

    # get page orientation from image size 
    orientation = 'P' if width < height else 'L'

    #  make sure image size is not greater than the pdf format size
    width = width if width < pdf_size[orientation]['w'] else pdf_size[orientation]['w']
    height = height if height < pdf_size[orientation]['h'] else pdf_size[orientation]['h']

    pdf.add_page(orientation=orientation)

    pdf.image(file, 0, 0, width, height)
pdf.output(output_name, "F")


